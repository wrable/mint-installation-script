#add vs-code rep
sudo apt install software-properties-common apt-transport-https wget -y
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

#add teams rep
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main" > /etc/apt/sources.list.d/teams.list'

#add spotify rep 
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

#add veracrypt rep
sudo add-apt-repository ppa:unit193/encryption -y

#add inkscape rep
sudo add-apt-repository ppa:inkscape.dev/stable -y

#add gimp rep
sudo apt install ppa-purge -y
sudo ppa-purge ppa:otto-kesselgulasch/gimp

#add audacity rep
sudo add-apt-repository ppa:ubuntuhandbook1/audacity -y
#add snapd
sudo rm /etc/apt/preferences.d/nosnap.pref

#update adn upgrade 
sudo apt update 
sudo apt upgrade -y

#install rest 
sudo apt install veracrypt -y
sudo apt install htop -y
sudo apt install code -y
sudo apt install teams -y
sudo apt install git -y 
sudo apt install vim -y
sudo apt install valgrind -y
sudo apt install clang -y 
sudo apt install gcc -y
sudo apt install g++ -y
sudo apt install libinput-tools -y #touchpad support
sudo apt install kicad -y
sudo apt install spotify-client -y
sudo apt install openfortivpn network-manager-fortisslvpn network-manager-fortisslvpn-gnome -y
sudo apt install snapd -y
sudo apt install screen -y
sudo snap install pycharm-community --classic
sudo snap install signal-desktop
sudo snap install slack --classic
#sudo snap install hiri #paid email client 
#install kvm libvirt
sudo apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virt-manager -y
#install wireshark
sudo apt install wireshark -y
#sudo dpkg-reconfigure wireshark-common #reconf wireshark
sudo apt install inkscape -y
sudo apt install gimp -y
sudo apt install audacity -y
sudo apt install obs-studio -y
sudo apt install flameshot -y
sudo apt install wine -y
sudo apt install cura -y
sudo apt install gcovr -y
sudo apt install hyphen-pl -y
sudo apt-get install aspell-pl -y
sudo apt install htop -y
sudo apt install conky -y
sudo cp conky.conf /etc/conky/conky.conf
flatpak install flathub com.teamspeak.TeamSpeak -y 
